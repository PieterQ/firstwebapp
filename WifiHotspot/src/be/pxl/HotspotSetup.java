package be.pxl;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HotspotSetup {

    public String outputString;

    public String executeCommand(String command) {
        StringBuffer output = new StringBuffer();
        Process processCommand;

        try {
            processCommand = Runtime.getRuntime().exec(command);
            String line;


            BufferedReader reader = new BufferedReader(new InputStreamReader(processCommand.getInputStream()));

            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

            outputString = output.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return outputString;
    }

    public boolean canHotspotBeCreated() throws Exception {
        boolean canBeEnabled;

        String showDrivers = executeCommand("netsh wlan show drivers");
        if (showDrivers.contains("Hosted network supported  : Yes")) {
            canBeEnabled = true;

        } else if (showDrivers.contains("Hosted network supported  : No")) {
            canBeEnabled = false;
        } else {
            throw new Exception("something went wrong");
        }
        return canBeEnabled;
    }

    public void setMobileHotspot(String ssid, String password){
        String commandString = "netsh wlan set hostednetwork mode=allow ssid=" + ssid + " key=" + password;
        System.out.println(executeCommand(commandString));
    }

    public void startMobileHotspot(){
        System.out.println(executeCommand("netsh wlan start hostednetwork"));
    }

    public void stopMobileHotspot(){
        System.out.println(executeCommand("netsh wlan stop hostednetwork"));
    }

    public String getConnections(){
        return executeCommand("netsh wlan show hostednetwork");
    }
}

