# 2018 AON11

An exam distribution tool for classrooms. 

This program will be able to run on a supervisor computer and distribute an exam through his own hotspot and monitor students connected to it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

To run the front-end locally you can use angular-CLI and issue the `ng serve -o` command. 

To run the back-end locally it is advised to use maven and issue the spring boot with the `mvn spring-boot:run` command. Before launching you will have to download the nodejs packages required for angular. These can be installed using `npm update`.  

### Prerequisites

What things you need to install the software.

##### Back-end
```
- Maven (https://maven.apache.org/download.cgi) 
- Tomcat or XAMPP (which comes with tomcat)
```

##### front-end
```
- Node.JS (https://nodejs.org/en/) 
- Angular-CLI (npm install -g @angular/cli
```

### Structure

- back-end
	- contains the maven project
	- the structure within the source folder is divided in 4 main directories
		- config (containing the main configuration of the application) 
		- model (all the entity models used by the application)
		- controller (handles sending the response to the 'user')
		- repository 
		- service (containing the functionality aspect of the entities)
- front-end
	- contains the Angular project
	- the basic rules of an Angular Typescript project structure is applied.

## Built With

* [Angular4](https://angular.io/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring](https://spring.io/) - The api framework used


## Authors

* **Ebert Joris** - [EbertJoris](https://github.com/EbertJoris)
* **Pieter Quintens** - [Coming Soon]()
* **Lukas Moors** - [Coming Soon]()
* **Michiel Smeets** - [Coming Soon]()


