package be.pxl.AON11.examentool.service;

import be.pxl.AON11.examentool.model.Skeleton;
import be.pxl.AON11.examentool.repository.SkeletonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
@Service("skeletonService")
@Transactional
public class SkeletonServiceImpl implements SkeletonService {

    private final SkeletonRepository skeletonRepository;

    @Autowired
    public SkeletonServiceImpl(SkeletonRepository skeletonRepository) {
        this.skeletonRepository = skeletonRepository;
    }

    @Override
    public Optional<Skeleton> findSkeletonById(Integer id) {
        return skeletonRepository.findById(id);
    }

    @Override
    public void saveSkeleton(Skeleton skeleton) {
        skeletonRepository.save(skeleton);
    }

    @Override
    public void updateSkeleton(Skeleton skeleton) {
        saveSkeleton(skeleton);
    }

    @Override
    public void deleteSkeletonById(Integer id) {
        skeletonRepository.deleteById(id);
    }

    @Override
    public List<Skeleton> findAllSkeletons() {
        return skeletonRepository.findAll();
    }

    @Override
    public boolean doesSkeletonExist(Integer skeletonId) {
        return skeletonRepository.existsById(skeletonId);
    }

    @Override
    public boolean doesSkeletonExistWithHref(String href) {
        return skeletonRepository.existsByHref(href);
    }
}

