package be.pxl.AON11.examentool.service;

import be.pxl.AON11.examentool.model.Course;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
public interface CourseService {

    Optional<Course> findCourseById(Integer id);

    void saveCourse(Course course);

    void updateCourse(Course course);

    void deleteCourseById(Integer id);

    List<Course> findAllCourses();

    boolean doesCourseExist(Integer courseId);

}
