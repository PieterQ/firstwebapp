package be.pxl.AON11.examentool.repository;

import be.pxl.AON11.examentool.model.Supervisor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lukas Moors on 30/03/2018.
 */
public interface SupervisorRepository extends JpaRepository<Supervisor, Integer> {


}
