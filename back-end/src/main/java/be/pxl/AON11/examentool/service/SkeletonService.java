package be.pxl.AON11.examentool.service;

import be.pxl.AON11.examentool.model.Skeleton;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
public interface SkeletonService {

    Optional<Skeleton> findSkeletonById(Integer id);

    void saveSkeleton(Skeleton skeleton);

    void updateSkeleton(Skeleton skeleton);

    void deleteSkeletonById(Integer id);

    List<Skeleton> findAllSkeletons();

    boolean doesSkeletonExist(Integer skeletonId);

    boolean doesSkeletonExistWithHref(String href);

}
