package be.pxl.AON11.examentool.service;

import be.pxl.AON11.examentool.model.Student;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
public interface StudentService {

    Optional<Student> findStudentById(Integer id);

    void saveStudent(Student student);

    void updateStudent(Student student);

    void deleteStudentById(Integer id);

    void deleteAllStudents();

    List<Student> findAllStudents();

    boolean doesStudentExist(Integer studentId);

    boolean doesStudentExistWithStudentNumber(Integer studentNumber);

}
