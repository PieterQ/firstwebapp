package be.pxl.AON11.examentool.repository;

import be.pxl.AON11.examentool.model.Skeleton;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
public interface SkeletonRepository extends JpaRepository<Skeleton, Integer> {

    boolean existsByHref(String href);
}
