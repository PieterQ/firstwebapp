package be.pxl.AON11.examentool.service;


import be.pxl.AON11.examentool.model.Supervisor;
import be.pxl.AON11.examentool.repository.SupervisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by Lukas Moors on 30/03/2018.
 */
@Service("supervisorService")
@Transactional
public class SupervisorServiceImpl implements SupervisorService {

    @Autowired
    private SupervisorRepository supervisorRepository;

    @Override
    public void saveSupervisor(Supervisor supervisor) {
        supervisorRepository.save(supervisor);
    }

    @Override
    public void updateSupervisor(Supervisor supervisor) {
        saveSupervisor(supervisor);
    }

    @Override
    public void deleteSupervisor() {
        supervisorRepository.deleteById(1);
    }

    @Override
    public Optional<Supervisor> findSupervisor() {
        return supervisorRepository.findById(1);
    }
}

