package be.pxl.AON11.examentool.controller;

import be.pxl.AON11.examentool.model.Supervisor;
import be.pxl.AON11.examentool.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by Lukas Moors on 30/03/2018.
 */
@RestController
@RequestMapping("/api/users/supervisor")
public class SupervisorController {

    //Service will do all data retrieval/manipulation work
    @Autowired
    SupervisorService supervisorService;

    @GetMapping("/")

    public Optional<Supervisor> getSupervisor() {
        return supervisorService.findSupervisor();
    }

    @PostMapping("/")

    public ResponseEntity<Supervisor> createSupervisor(@RequestBody Supervisor supervisor) {

        if (supervisorService.findSupervisor().isPresent()) {
            return new ResponseEntity<Supervisor>(HttpStatus.CONFLICT);
        } else {
            supervisorService.saveSupervisor(supervisor);
            return new ResponseEntity<Supervisor>(supervisor, HttpStatus.CREATED);
        }
    }

    @DeleteMapping("/")

    public void deleteSupervisor() {
        supervisorService.deleteSupervisor();
    }

}