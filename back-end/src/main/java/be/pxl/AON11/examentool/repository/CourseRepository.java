package be.pxl.AON11.examentool.repository;

import be.pxl.AON11.examentool.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ebert Joris on 03/04/2018.
 */
public interface CourseRepository extends JpaRepository<Course, Integer> {

}
