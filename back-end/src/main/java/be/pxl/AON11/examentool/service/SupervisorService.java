package be.pxl.AON11.examentool.service;

import be.pxl.AON11.examentool.model.Supervisor;

import java.util.List;
import java.util.Optional;

/**
 * Created by Lukas Moors on 30/03/2018.
 */

public interface SupervisorService {

    void saveSupervisor(Supervisor supervisor);
    void updateSupervisor(Supervisor supervisor);
    void deleteSupervisor();

    Optional<Supervisor> findSupervisor();
}
