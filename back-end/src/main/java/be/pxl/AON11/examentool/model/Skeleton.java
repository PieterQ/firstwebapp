package be.pxl.AON11.examentool.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Ebert Joris on /03/2018.
 */
@Entity
@Table(name= "skeletons", catalog = "examentool")
public class Skeleton {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String href;

    @OneToOne(mappedBy = "skeleton")
    private Course course;

    public Skeleton() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
