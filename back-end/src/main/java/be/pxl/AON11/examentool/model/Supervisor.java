package be.pxl.AON11.examentool.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Lukas Moors on 30/03/2018.
 */
@Entity
@Table(name = "supervisors", catalog = "examentool")
public class Supervisor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String password;



    public Supervisor() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
            this.id = 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
