package be.pxl.AON11.examentool.repository;

import be.pxl.AON11.examentool.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {

    boolean existsByStudentNumber(Integer studentNumber);

}
