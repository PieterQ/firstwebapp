package be.pxl.AON11.examentool.controller;

import be.pxl.AON11.examentool.model.Student;
import be.pxl.AON11.examentool.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
@RestController
@RequestMapping("/api/users/students")
@CrossOrigin(origins = "http://localhost:4200")
public class StudentController {

    //Service will do all data retrieval/manipulation work
    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    public List<Student> getAll() {
        return studentService.findAllStudents();
    }

    @GetMapping("/{studentId}")

    public ResponseEntity<Student> getStudentById(@PathVariable Integer studentId) {
        Optional<Student> student = studentService.findStudentById(studentId);

        if (student.isPresent()) {
            return new ResponseEntity<>(student.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/")

    public ResponseEntity<Student> createStudent(@RequestBody Student student) {

        // Check if user with same student number already exists
        // This should not be allowed because it's a unique value.
        if (!studentService.doesStudentExistWithStudentNumber(student.getStudentNumber())) {
            studentService.saveStudent(student);
            return new ResponseEntity<>(student, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/{studentId}")

    public ResponseEntity<Student> updateStudentById(@PathVariable Integer studentId, @RequestBody Student student) {

        if (studentService.doesStudentExist(studentId)) {
            // We set the id to the updated id otherwise it could overwrite another one.
            student.setId(studentId);
            studentService.updateStudent(student);
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }

    @DeleteMapping("/{studentId}")

    public ResponseEntity<Student> deleteStudentById(@PathVariable Integer studentId) {

        if (studentService.doesStudentExist(studentId)) {
            studentService.deleteStudentById(studentId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }
}
