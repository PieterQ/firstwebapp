package be.pxl.AON11.examentool.controller;

import be.pxl.AON11.examentool.model.Course;
import be.pxl.AON11.examentool.model.Skeleton;
import be.pxl.AON11.examentool.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
@RestController
@RequestMapping("/api/courses/")
@CrossOrigin(origins = "http://localhost:4200")
public class CourseController {

    //Service will do all data retrieval/manipulation work
    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Course>> getAll() {
        return new ResponseEntity<>(courseService.findAllCourses(), HttpStatus.OK);
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<Course> getCourseById(@PathVariable Integer courseId) {
        Optional<Course> course = courseService.findCourseById(courseId);

        if(course.isPresent()) {
            return new ResponseEntity<>(course.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/")
    public ResponseEntity<Course> postCourse(@RequestPart Course course) {
        // Check if file is not empty and if it is a zip
        if(!courseService.doesCourseExist(course.getId())) {
            courseService.saveCourse(course);
            return new ResponseEntity<>(course, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PutMapping("/{courseId}")
    public ResponseEntity<Course> updateCourseById(@PathVariable Integer courseId,
                                                       @RequestBody Course course) {

        if (courseService.doesCourseExist(courseId)) {
            // We set the id to the updated id otherwise it could overwrite another one.
            course.setId(courseId);
            // Update the entity in the database
            courseService.updateCourse(course);
            return new ResponseEntity<>(course, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }

    @DeleteMapping("/{courseId}")
    public ResponseEntity<Course> deleteCourseById(@PathVariable Integer courseId) {

        if (courseService.doesCourseExist(courseId)) {
            courseService.deleteCourseById(courseId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }

    @GetMapping("/{courseId]/skeleton/")
    public ResponseEntity<Skeleton> getSkeletonOfCourse(@PathVariable Integer courseId) {
        if(courseService.doesCourseExist(courseId)) {
            Optional<Course> course = courseService.findCourseById(courseId);

            if(course.isPresent()) {
                Skeleton skeleton = course.get().getSkeleton();

                return new ResponseEntity<>(skeleton, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
