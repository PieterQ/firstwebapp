package be.pxl.AON11.examentool;

import be.pxl.AON11.examentool.config.JpaConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(JpaConfig.class)
@SpringBootApplication(scanBasePackages = "be.pxl.AON11.examentool")
public class ExamenToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenToolApplication.class, args);
	}
}
