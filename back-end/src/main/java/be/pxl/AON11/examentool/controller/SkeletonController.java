package be.pxl.AON11.examentool.controller;

import be.pxl.AON11.examentool.model.Skeleton;
import be.pxl.AON11.examentool.service.SkeletonService;
import be.pxl.AON11.examentool.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Ebert Joris on 24/03/2018.
 */
@RestController
@RequestMapping("/api/files/skeletons")
@CrossOrigin(origins = "http://localhost:4200")
public class SkeletonController {

    //Service will do all data retrieval/manipulation work
    private SkeletonService skeletonService;
    private final StorageService storageService;

    @Autowired
    public SkeletonController(SkeletonService skeletonService,
                              StorageService storageService) {
        this.skeletonService = skeletonService;
        this.storageService = storageService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Skeleton>> getAll() {
        return new ResponseEntity<>(skeletonService.findAllSkeletons(), HttpStatus.OK);
    }

    @GetMapping("/{skeletonId}")
    public ResponseEntity<Skeleton> getSkeletonById(@PathVariable Integer skeletonId) {
        Optional<Skeleton> skeleton = skeletonService.findSkeletonById(skeletonId);

        if(skeleton.isPresent()) {
            return new ResponseEntity<>(skeleton.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/file/{skeletonId}")
    public ResponseEntity<InputStreamResource> getSkeletonFileById(@PathVariable Integer skeletonId) throws IOException {
        Optional<Skeleton> skeleton = skeletonService.findSkeletonById(skeletonId);

        if (skeleton.isPresent()) {
            Resource file = storageService.loadFile(skeleton.get().getHref());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/zip"));
            headers.add("Access-Control-Allow-Origin", "*");
            headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
            headers.add("Access-Control-Allow-Headers", "Content-Type");
            headers.add("Content-Disposition", "filename=" + file.getFilename());
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

            headers.setContentLength(file.contentLength());
            return new ResponseEntity<>(
                    new InputStreamResource(file.getInputStream()), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/")
    public ResponseEntity<Skeleton> postSkeleton(@RequestParam("file") MultipartFile file,
                                                         @RequestPart Skeleton skeleton) {
        // Check if file is not empty and if it is a zip
        if(!file.isEmpty()) {

            // Do not create skeleton if it already exist
            if (!skeletonService.doesSkeletonExistWithHref(skeleton.getHref())) {

                String skeletonLocation = "skeletons/";
                String skeletonName = skeleton.getCourse().getName().replace(' ', '_') + ".zip";

                // the path for saving the skeleton
                storageService.store(file, skeletonLocation, skeletonName);

                // the path to download the skeleton
                skeleton.setHref(storageService.rootLocation + "/skeletons/" + skeletonName);

                // save the skeleton in the database
                skeletonService.saveSkeleton(skeleton);

                return new ResponseEntity<>(skeleton, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PutMapping("/{skeletonId}")
    public ResponseEntity<Skeleton> updateSkeletonById(@PathVariable Integer skeletonId,
                                                       @RequestBody Skeleton skeleton) {

        if (skeletonService.doesSkeletonExist(skeletonId)) {
            // We set the id to the updated id otherwise it could overwrite another one.
            skeleton.setId(skeletonId);
            // Update the entity in the database
            skeletonService.updateSkeleton(skeleton);
            return new ResponseEntity<>(skeleton, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }

    @DeleteMapping("/{skeletonId}")
    public ResponseEntity<Skeleton> deleteSkeletonById(@PathVariable Integer skeletonId) {

        if (skeletonService.doesSkeletonExist(skeletonId)) {
            skeletonService.deleteSkeletonById(skeletonId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

    }
}
