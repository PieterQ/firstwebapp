import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import { Student } from '../model/student.module';

@Injectable()
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable());

  constructor(
    private router: Router
  ) { }

  loginStudent(student: Student) {
    if (student.name !== '' &&
       student.studentNumber.toString() !== '' &&
       student.course !== '') {
        this.loggedIn.next(true);
        this.router.navigate(['/student']);
       }
  }

  logout() {
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }

  private tokenAvailable(): boolean {
    return !!localStorage.getItem('userLoggedIn');
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
}
