export class Student {

  name: string;
  studentNumber: number;
  course: string;
  id: number;

  constructor(name: string,
              studentNumber: number,
              course: string,
              id?: number) {
                  this.name = name;
                  this.studentNumber = studentNumber;
                  this.course = course;
                  this.id = id;
              }
}
