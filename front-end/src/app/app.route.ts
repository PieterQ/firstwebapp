import { AppComponent } from './app.component';
import { Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './login/login.component';
import { StudentComponent } from './student/student.component';

// Definition of the route variables
// export this so that it can be imported in app.module.ts
export const routes: Routes = [
    // Path is the same as the url behind the base url (see index.html)
    // component is the component that is loaded in the routerOutlet (see app.component.html)
    { path: '', component: AppComponent, canActivate: [AuthGuard] },
    { path: 'student', component: StudentComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent }
];
